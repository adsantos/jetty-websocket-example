import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;

/**
 * This class starts a Jetty Server which accepts the WebSocket connections and
 * calls the ChatWebSocketHandler
 * 
 * @author pania
 * 
 */
public class ChatWebSocketServer {

	public static void main(String[] args) {
		try {
			// Create a Jetty server with the 8081 port.
			Server server = new Server(8081);

			// Register the ChatWebSocketHandler
			ChatWebSocketHandler chatWebSocketHandler = new ChatWebSocketHandler();
			// Pass the DefaultHandler so 404 errors will be handeld correctly.
			chatWebSocketHandler.setHandler(new DefaultHandler());
			server.setHandler(chatWebSocketHandler);

			// Start the Jetty server.
			server.start();
			server.join();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
