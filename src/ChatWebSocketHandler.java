import java.io.IOException;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.jetty.websocket.WebSocket;
import org.eclipse.jetty.websocket.WebSocketHandler;

/**
 * This class implements the handling of the WebSocket Requests. The outer class
 * only implements the doWebSocketConnect method which returns a class which
 * implements the WebSocket interface.
 * 
 * @author pania
 * 
 */
public class ChatWebSocketHandler extends WebSocketHandler {

	/**
	 * A threadsafe list of open WebSockets
	 */
	private final Set<ChatWebSocket> webSockets = new CopyOnWriteArraySet<ChatWebSocket>();

	/**
	 * This method will be called on every client connect. This method must
	 * return a WebSocket-implementing-class to work on. WebSocket is just an
	 * interface with different types of communication possibilities. You must
	 * create and return a class which implements the necessary WebSocket
	 * interfaces.
	 */
	public WebSocket doWebSocketConnect(HttpServletRequest request,
			String protocol) {
		return new ChatWebSocket();
	}

	/**
	 * Implements an anonymous class for the websocket with the necessary
	 * WebSocket interfaces and the network logic.
	 * 
	 * @author pania
	 * 
	 */
	private class ChatWebSocket implements WebSocket.OnTextMessage {
		/**
		 * The connection object of every WebSocket.
		 */
		private Connection connection;

		/**
		 * This method is defined by the WebSocket interface. It will be called
		 * when a new WebSocket Connection is established.
		 */
		public void onOpen(Connection connection) {
			System.out.println("[SERVER] Opened connection");
			// WebSocket has been opened. Store the opened connection
			this.connection = connection;
			// Add ChatWebSocket in the global list of ChatWebSocket instances
			webSockets.add(this);
		}

		/**
		 * This method is defined by the WebSocket.OnTestMessage Interface. It
		 * will be called when a new Text message has been received. In this
		 * class, we will send the received message to all connected clients.
		 */
		public void onMessage(String data) {
			System.out.println("[SERVER] Received data: " + data);
			// Iterate over every connected client and send the received message
			// to him.
			try {
				for (ChatWebSocket webSocket : webSockets) {
					// send a message to the current client WebSocket.
					webSocket.connection.sendMessage(data);
				}
			} catch (IOException x) {
				// Error was detected, close the ChatWebSocket client side
				System.out.println(x.getStackTrace());
				this.connection.close();
			}
		}

		/**
		 * This method is defined by the WebSocket Interface. It will be called
		 * when a WebSocket Connection is closed.
		 */
		public void onClose(int closeCode, String message) {
			System.out.println("[SERVER] Client has closed the connection");
			// Remove ChatWebSocket in the global list of ChatWebSocket
			// instance.
			webSockets.remove(this);
		}
	}
}