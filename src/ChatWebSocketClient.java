import java.net.URI;
import java.net.URISyntaxException;

import de.roderick.weberknecht.WebSocket;
import de.roderick.weberknecht.WebSocketConnection;
import de.roderick.weberknecht.WebSocketEventHandler;
import de.roderick.weberknecht.WebSocketException;
import de.roderick.weberknecht.WebSocketMessage;

/**
 * This class implements a very basic chat client which is talking via
 * WebSockets with the ChatWebSocketServer. The most code is taken from the
 * WebSocket example of the WeberKnecht library homepage.
 * 
 * @author pania
 * 
 */
public class ChatWebSocketClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			URI url = new URI("ws://127.0.0.1:8081/");
			WebSocket websocket = new WebSocketConnection(url);

			// Register Event Handlers
			websocket.setEventHandler(new WebSocketEventHandler() {
				@Override
				public void onOpen() {
					System.out.println("[CLIENT] Opened connection");
				}

				@Override
				public void onMessage(WebSocketMessage message) {
					System.out.println("[CLIENT] Received message: "
							+ message.getText());
				}

				@Override
				public void onClose() {
					System.out.println("[CLIENT] Closed connection");
				}
			});

			// Establish WebSocket Connection
			websocket.connect();

			// Send UTF-8 Text
			websocket.send("hello world");

			// Wait for the response to see the non-blocking behavior and the
			// reply from the server.
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			// Close WebSocket Connection
			websocket.close();

			// Catch exceptions that relate to the WebSocket connection
			// (connection refused etc.)
		} catch (WebSocketException wse) {
			wse.printStackTrace();
		} catch (URISyntaxException use) {
			use.printStackTrace();
		}

	}

}
